"""
Fabfile for deployment, requiring fabric 1.0
(incompatible with older versions)
"""

from __future__ import with_statement

import fabric
from fabric.api import *
from fabric.operations import local
from fabric.contrib import console, files

import time
import os
import platform

import settings as settings_django
import settings_project as settings_project
try:
    import settings_secret as settings_secret
except ImportError:
    print "Warning: local settings_secret.py not found!"

env.project_name = settings_project.PROJECT_NAME

env.virtualenv_dir = 'env'
env.command_join = '&&'
    
# defaults
env.db_host = 'localhost'
env.db_superuser = 'postgres'

env.project_root = os.getcwd()
env.path = env.project_root

# suppress dumping of individual commands
# fabric.state.output.running = False

if platform.system() == "Windows":
    virtualenv_bin = '%s\\Scripts\\' % env.virtualenv_dir
else:
    virtualenv_bin = '%s/bin/' % env.virtualenv_dir

local_python = '%spython' % virtualenv_bin

# ---------------------------------------------------------
# Deployment configs
# ---------------------------------------------------------

def on(host):
    assert getattr(settings_secret, 'HOSTS', False), "No HOSTS defined in settings_secret.py"
    assert settings_secret.HOSTS.get(host, False), "HOSTS['%s'] needs to be defined in settings_secret.py" % host
    env.host_settings = settings_secret.HOSTS[host]
    env.server_name = host
    assert env.host_settings.get('host',False), "HOSTS['%s']['host'] not set in settings_secret.py" % host
    if env.host_settings.get('user', None):
        env.user = env.host_settings['user']
    env.hosts = [env.host_settings['host']]
    env.path = env.host_settings['path']
    
    
# ---------------------------------------------------------
# Setup
# ---------------------------------------------------------

def init():
    """
    Call this on the first contact of a project after starting, checking out or deploying to a new server.    
    """
    if not env.hosts:
        _init_local()
    else:
        _init_remote()


def _init_local():
    """
    Initialize local development environment. Call this after your first checkout of the project.

    If the project has not been bootstrapped (i.e. it is a newly created project), this is done here (setting the project name).
    In any case, requirements are installed, the local database is set up and the settings_secret.py file is generated.
    """
    if env.hosts:
        abort('Error: _init_local called on remote host')

    create_virtualenv()
    install_requirements()
    
    create_secret_settings()

    local('mkdir -p releases')
    
    syncdb()
    create_migration(app_name="imscam", initial=True)
    load_data(filename='default_entries.json')
    #syncdb()
    #local('%s manage.py schemamigration imscam --initial' % (local_python), capture=False)
    #local('%s manage.py migrate imscam' % (local_python), capture=False)
    #load_data(filename='default_entries.json')


def create_secret_settings():
    "Creates a settings_secret.py file in the target location"
    
    if env.hosts:    
        filename = 'settings_secret.py.temp'
        if files.exists('%s/settings_secret.py' % (env.path)):
            if not console.confirm('Warning: settings_secret.py exists in target location - overwrite?', default=False):
                return            
            run('rm %s/settings_secret.py' % (env.path))
        email_host = env.host_settings and env.host_settings.get('email_host','') or ''
    else:
        filename = 'settings_secret.py'
        if os.path.exists(filename):
            abort('%s exists - aborting!' % filename)
        email_host = ''
        

    import random
    secret_key = ''.join([random.choice("abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)") for i in range(50)])

    db_engine = 'django.db.backends.sqlite3'

    content = """
# This file contains settings that should never be made public
# Do not check in to your VCS!
# You can also override settings here to reflect your local development environment

DJANGO_PROJECT_ROOT = '%(project_root)s'
DJANGO_RELEASE_ROOT = '%(release_root)s'
  
SECRET_KEY = '%(secret_key)s'

EMAIL_HOST = '%(email_host)s'
EMAIL_HOST_PASSWORD = '%(email_pass)s'
    
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'sqlite.db',
        'HOST': '',
        'PORT': '',
        'USER': '',
        'PASSWORD': '',
    }
}

HOSTS = {
    'test': {
        'host': 'pi@host:port',
        'user': 'pi',
        'path': '/home/pi/%(project_name)s',
        'make_folder_world_writeable' : None,
        'virtualenv_version': (1,4,9),
    }
}

STATICFILES_DIRS = [DJANGO_RELEASE_ROOT + 'static/']

# most common options for development:

# DEBUG = True
# TEMPLATE_DEBUG = True    
# SERVE_STATIC = True
# MEDIA_URL = '/media/'
# STATIC_URL = '/static/'
# EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

""" % {
    'project_root': env.path + '/',
    'release_root': (env.path + '/') if not env.hosts else (env.path + '/current-release/'),
    'secret_key': secret_key,
    'email_host': email_host,
    'email_pass': '',
    'project_name': env.project_name
}
    
        
    file = open(filename,'w')
    file.write(content)
    file.close()

    if env.hosts:       
        put('settings_secret.py.temp', '%s/settings_secret.py' % (env.path))
        if platform.system() == "Windows":
            local('rem settings_secret.py.temp')
        else:
            local('rm settings_secret.py.temp')


# ---------------------------------------------------------
# Remote setup
# ---------------------------------------------------------

def prepare_host():
    packages = [
        #'fabric',
        'python-dev',
        'python-pip',
        'python-virtualenv',
        #'python-opencv',
        #'python-pygame',
        #'python-numpy',
        #'python-scipy',
        'git',
        'mercurial'
    ]
    _sudo('apt-get update')
    for package in packages:
        _sudo('apt-get install %s' % package)

    # convert uploaded wrongly encoded filenames using
    # sudo apt-get install convmv
    # sudo convmv -r -f ISO-8859-1 -t utf8 --preserve-mtimes --notest cloudless
        
def _init_remote():
    """
    Initialize project environment on server.    
    """
    require('path', provided_by = [on])

    create_project_dir()
    deploy_nosyncdb()
    create_virtualenv()
    install_requirements()
    create_db()
    create_secret_settings()
    syncdb()
    create_migration(app_name="imscam", initial=True)
    load_data(filename='default_entries.json')
    createsuperuser()
    install_site()
    reload()


def create_project_dir():
    """
    Create directory structure for project including parent directories on remote server.
    """
    with settings(warn_only=True):
        run('mkdir -p %s/packages' % (env.path,))
        run('mkdir %s/log' % (env.path,))
        run('mkdir -p %s/media' % (env.path,))
        run('mkdir -p %s/collected_static' % (env.path,))
    # change permissions for writable folder
    cmd = env.host_settings.get('make_folder_world_writeable','chown -R www-data:www-data')
    if cmd:
        run('%s %s/media' % (cmd, env.path))
# do we need this?
#        run('%s %s/collected_static' % (cmd, env.path))

def create_virtualenv():
    if env.hosts:
        ver = env.host_settings.get('virtualenv_version', (1,4,9))
        if ver[0] > 1 or (ver[0] == 1 and ver[1] >= 7):
            # new version requires --system-site-packages flag
            run ('cd %(path)s && virtualenv --system-site-packages env' % env)
        else:
            # in old version it is the default behaviour
            run ('cd %(path)s && virtualenv env' % env)
    else:
# do we need this?
#        result = ''
        with settings(warn_only=True):
            result = local('virtualenv --system-site-packages env', capture=True)
        if result.failed:
            # old version has system-site-packages as default and no flag
            local('virtualenv env')
    

def install_requirements():
    """
    Install required software as found in requirements.txt (using pip).    
    """
    if env.hosts:
        run ('cd %(path)s %(command_join)s env/bin/pip install -r current-release/requirements.txt' % env)
    else:
        local('%spip install -r requirements.txt' % virtualenv_bin, capture=False)
        
        
def create_db():
    """
    Create database and db role for the project.
    """ 
    if console.confirm('Create database %s?' % env.project_name, default=False):
        env.db_user = prompt('DB user for %s:' % env.host, default=env.project_name)
        env.db_password = prompt('DB password for user %s:' % env.db_user)
            
        # -e echo-sql S no-superuser D no-createdb R no-createrole l can-login
        # P prompt-for-passwd -U <login role> -O <owner role> -h <hostname>
        # TODO find a way to use provided password! (use SQL instead of command)
        run('createuser -e -SDRlP -U %s -h %s %s' % (env.db_superuser, env.db_host, env.db_user))    
        # -U <login role> -O <owner role> -h <hostname>
        run('createdb -e -E UTF8 -O %s -U %s -h %s %s' % (env.db_user, env.db_superuser, env.db_host, env.project_name))


def createsuperuser():
    require('hosts', provided_by = [staging])
    if console.confirm('Create superuser?', default=False):
        # we are in a symlinked directory so we have to add one more '../' !
        with cd('%(path)s/current-release/' % env):
            run('../../env/bin/python manage.py createsuperuser --username=admin --email=ledermann@ims.tuwien.ac.at')


# ---------------------------------------------------------
# Regular Deployment
# ---------------------------------------------------------

# TODO allow package name to be specified externally to tag releases
def package():
    "Package the current state into a tarball for deployment."
    project_path = env.path
    env.path = ''
    env.release = time.strftime('%Y-%m-%d.%H%M%S')
    if platform.system() == "Windows":
        # xcopy cannot copy into a subdirectory even if it is excluded, so we
        # have to make a diversion via the temp directory
        local('xcopy . %%TEMP%%\\releases\\%s\\ /e /c /q /exclude:.deploy-ignore-win'
              % env.release)
        local('xcopy %%TEMP%%\\releases\\%s releases\\%s\\ /e /c /q'
              % (env.release, env.release))
        # requires tar and gzip for windows http://sourceforge.net/projects/unxutils/
        local('tar cf releases/%s.tar -C releases/%s/ .' % (env.release, env.release))    
        local('gzip releases\\%s.tar' % env.release)
         # local('copy releases\\%s.tar.gz releases\\latest.tar.gz' % env.release)
        local('rmdir /s /q releases\\%s\\' % env.release)
    else:
        local('tar --exclude-from .deploy-ignore -zcf releases/%s.tar.gz *' % env.release)
        
    env.path = project_path

def upload():
    "Upload current build."
    require('path', provided_by = [staging])   
    require('release', provided_by = [package])      
    put('releases/%s.tar.gz' % env.release, '%(path)s/packages/%(release)s.tar.gz' % env)
    run('mkdir -p %s/releases/%s' % (env.path, env.release))
    run('cd %s/releases/%s && tar zxf ../../packages/%s.tar.gz' % (env.path, env.release, env.release))
    # create symbolic link for secret settings
    run('cd %s/releases/%s; ln -s ../../settings_secret.py settings_secret.py' % (env.path, env.release))
    
    # create symbolic link for admin and app media
    # run('cd %s/releases/%s; ln -s ../../../env/lib/python2.6/site-packages/django/contrib/admin/media/ media/admin' % (env.path, env.release))
    #for app_path in settings_project.APP_MEDIA:
    #    run('cd %s/releases/%s; ln -s ../../../%s media/%s' % (env.path, env.release, app_path[1], app_path[0]))
    
    # symlink uploads dir
    #run('cd %s/releases/%s; ln -s ../../../uploads/ media/uploads' % (env.path, env.release))
    if platform.system() == "Windows":
        local('del releases\\%s.tar.gz' % env.release)
    else:
        local('rm releases/%s.tar.gz' % env.release)

def collect_static():
    if not env.hosts:
        local('%s manage.py collectstatic --noinput' % local_python, capture=False) #-v0 
    else:
        #with settings(warn_only=True):
            # hack: delete file to be overridden by our own version
        #    run('cd %(path)s && rm collected_static/grappelli/js/SelectFilter2.js' % env)
        # we are in a symlinked directory so we have to add one more '../' !
        run('cd %(path)s/current-release && ../../env/bin/python manage.py collectstatic --noinput' % env) 

def activate():
    "Activate (symlink) our current release"
    require('release', provided_by=[package])
    with settings(warn_only=True):
        run('cd %(path)s; rm releases/previous' % env)
        run('cd %(path)s; mv releases/current releases/previous' % env)
        run('cd %(path)s; rm current-release' % env)
    run('cd %(path)s; ln -s releases/%(release)s releases/current' % env)   
    run('cd %(path)s; ln -s releases/%(release)s current-release' % env)   

def reload():
    "Reload the web server"
    with _frame('reloading web server'):
        _sudo('/etc/init.d/apache2 reload')

def deploy_nosyncdb():
    "Build the project and deploy it to a specified environment."
    require('hosts', provided_by = [staging])   
    package()
    upload()
    activate()

def deploy_noreload():
    "Build the project and deploy it to a specified environment."
    deploy_nosyncdb()
    syncdb()
    collect_static()

def deploy():
    "Build the project and deploy it to a specified environment, reload web server"
    deploy_noreload()
    if env.host_settings.get('needs_reload',True):
        reload()

def deploy_requirements():
    "Deploy project, update requirements, reload web server"
    deploy_nosyncdb()
    install_requirements()
    syncdb()
    collect_static()
    if env.host_settings.get('needs_reload',True):
        reload()

def syncdb():
    if not env.hosts:
        local('%s manage.py syncdb' % local_python, capture=False)
        #local('%s manage.py migrate' % local_python, capture=False)
    else:
        # we are in a symlinked directory so we have to add one more '../' !
        run('cd %(path)s/current-release %(command_join)s ../../env/bin/python manage.py syncdb --noinput' % env) 
        #run('cd %(path)s/current-release %(command_join)s ../../env/bin/python manage.py migrate' % env) 

def create_migration(app_name=None, manual=False, initial=False):
    if not app_name:
        abort('Please specify an app name like create_migration:<appname>')
        
    if not manual:
        auto_str = ' --auto'
    else:
        auto_str = ''
    
    if initial:
        auto_str = ' --initial'
    
    #migration_name = prompt('Migration Name: ', validate=r'^[a-z_0-9]+$')
    #local('%s manage.py schemamigration %s %s %s' % (local_python, app_name, migration_name, auto_str), capture=False)
    local('%s manage.py schemamigration %s %s' % (local_python, app_name, auto_str), capture=False)
    #if console.confirm('Please review migration code; Apply now?'):
    if initial:
        local('%s manage.py migrate %s --fake' % (local_python, app_name), capture=False)
    else:
        local('%s manage.py migrate %s' % (local_python, app_name), capture=False)

def translate():
    makemessages()
    if not console.confirm('Please review/edit translation files - continue?'):
        abort("Aborting on user request")    
    compilemessages()
    
def makemessages():
    if env.hosts:
        abort('Please create translations locally')
    for lang in settings_django.LANGUAGES:
        local('cd templates %s django-admin.py makemessages -l %s -e html -e txt' % (env.command_join, lang[0]), capture=False)
        for app_path in settings_project.TRANSLATE_APPS:
            local('cd %s %s django-admin.py makemessages -l %s -e html -e txt' % (app_path, env.command_join, lang[0]), capture=False)

def compilemessages():
    if env.hosts:
        abort('Please create translations locally')
    local('cd templates %s django-admin.py compilemessages' % (env.command_join, ), capture=False)
    for app_path in settings_project.TRANSLATE_APPS:
        local('cd %s %s django-admin.py compilemessages' % (app_path, env.command_join, ), capture=False)

def runserver():
    cmd = 'runserver 0.0.0.0:8000'
    if 'grappelli' in settings_django.INSTALLED_APPS and not 'django.contrib.staticfiles' in settings_django.INSTALLED_APPS:
        cmd += ' --adminmedia=env/lib/python2.6/site-packages/grappelli/static/grappelli'
    local('%s manage.py %s' % (local_python, cmd), capture=False)
    
    
def testserver(fixture='testdata'):
    local('python manage.py testserver fixtures\\%s' % fixture)


def runscheduler():
    cmd = 'scheduler'
    local('%s manage.py %s' % (local_python, cmd), capture=False)
    
    
# ---------------------------------------------------------
# testing
# ---------------------------------------------------------

def test(flags=''):

    flags = flags.split(',')
    warn = '' if 'nowarn' in flags else '-Wall'
    coverage = not ('nocoverage' in flags)
        
    with settings(warn_only=True):
        with hide('warnings'):
            if coverage:
                result = local('python %s -m coverage run --source="src" manage.py test %s' % (warn, ' '.join(settings_project.TEST_APPS)), capture=False)
            else:
                result = local('python %s manage.py test %s' % (warn, ' '.join(settings_project.TEST_APPS)), capture=False)
            if result.return_code == 0: 
                if coverage:
                    local('coverage report -m --omit=*/migrations/*')
                print "Tests passed!"
            elif result.return_code == 1: 
                print "Tests failed!"
            else: 
                print "Unknown return code!"
                raise SystemExit() 

# ---------------------------------------------------------
# data handling
# ---------------------------------------------------------

# sql fix for windows filename issues, see https://code.djangoproject.com/ticket/8593
# UPDATE cloudless_media SET image=lower(image), movie=lower(movie), thumbnail=lower(thumbnail), hover_thumbnail=lower(hover_thumbnail);

def dump_data(filename='testdata.json'):
    "Dump the applications data into a fixture using manage.py dumpdata"
    local('%s manage.py dumpdata --indent=2 > fixtures\\%s' % (local_python, filename))

def load_data(filename='testdata.json'):
    "Load application data from a fixture using manage.py loaddata"
    
    if not env.hosts:
        local('%s manage.py loaddata %s' % (local_python, filename), capture=False)
        #local('%s manage.py migrate' % local_python, capture=False)
    else:
        # we are in a symlinked directory so we have to add one more '../' !
        run('cd %(path)s/current-release %(command_join)s ../../env/bin/python manage.py loaddata %s' % (env, filename))
        
    
def dump_db(db_name=env.project_name):
    require('hosts', provided_by = [staging])   
    with _frame('Issue these commands manually:') as f:
        # -O ... no owner
        # -D ... explicit INSERT statements
        # -W ... force password prompt
        f.run('cd %s && pg_dump -E utf8 -f %s-dump.sql -O -D -W -h %s -U %s %s' % (env.path, db_name, env.db_host, db_name, db_name)) 
    
def copy_db(from_db, to_db):
    require('hosts', provided_by = [staging])   
    with _frame('Issue these commands manually:') as f:
        dump_db(from_db)
        #f.run('cd %s; pg_dump -E utf8 -f %s-dump.sql --no-owner -h %s -U %s -W %s' % (env.path, from_db, env.db_host, from_db, from_db)) 
        f.run('dropdb -h %s -U %s %s' % (env.db_host, env.db_superuser, to_db)) 
        f.run('createdb -e -E UTF8 -O %s -U %s -h %s %s' % (to_db, env.db_superuser, env.db_host, to_db))
        f.run('cd %s; psql -h %s -d %s -U %s -f %s-dump.sql' % (env.path, env.db_host, to_db, to_db, from_db)) 
        f.run('cd %s; rm %s-dump.sql' % (env.path, from_db)) 

def fetchdata(appname=''):
    require('hosts', provided_by = [on])
    with settings(warn_only=True):
        run('mkdir %s/fixtures' % (env.path,))
    if appname:
        excludes_str = ''
        fixture_name = '%s_%s.json' % (appname, datestr)
    else:
        excludes = [
            'contenttypes'
            'sessions'
            'auth.permission'
            'auth.message'
            'admin'
            'south'
            'registration'
            'invitation'
        ]
        excludes_str = ' --exclude='.join(excludes)
        fixture_name = 'all_%s.json' % (datestr,)
    run('cd %s/current-release; ../../env/bin/python manage.py dumpdata --natural --indent 2 %s %s > ../../fixtures/%s' % (env.path, excludes_str, appname, fixture_name))
    with settings(warn_only=True):
        local('mkdir fixtures')
    get('%s/fixtures/%s' % (env.path, fixture_name), 'fixtures/%s' % fixture_name)

def loaddata(appname):
    local(os.path.join(env.ve_prefix,'python manage.py loaddata fixtures/%s.json' % appname), capture=False)

# ---------------------------------------------------------
# backup
# ---------------------------------------------------------


def backup():
    # we need to back up these things:
    # - database
    # - current release including (frozen) requirements
    # - media / uploads (should be symlinked in current-release/media/ anyway)
    # - settings_secret.py (also symlinked in current-release)
    require('backup_home', provided_by = [on])
    env.timestamp = time.strftime('%Y-%m-%d')
    backup_path = '%s%s/%s/' % (env.backup_home, env.project_name, timestamp)
    # -p ... make parent dirs as needed
    run('mkdir -p %s' % backup_path)
    dump_db(filename='%s%s-dump.sql' % (backup_path, env.project_name))
    # -L ... always follow symbolic links
    # -p ... preserve=mode,ownership,timestamps
    # -R ... recursive
    _sudo('cp -LpR %s/current-release %s' % (env.path, backup_path))
    _sudo('cd %s; tar czf ../%s-BACKUP-%s.tar.gz *' % (backup_path, env.project_name, timestamp))
    

# ---------------------------------------------------------
# helper functions
# ---------------------------------------------------------

def _sudo(*args, **kwargs):
    """
    Perform sudo with special username, if set.
    """
    if env.host_settings and 'sudo_user' in env.host_settings:
        old_user = env.user
        env.user = env.host_settings['sudo_user']
        sudo(*args, **kwargs)
        env.user = old_user
    else:
        sudo(*args, **kwargs)


class _frame:
    "Frame console output for increased visibility. Also provides methods for printing 'fake' commands to console."
    
    current_frame = None
    
    def __init__(self, message=None, width=75, wait=None, wait_prompt='Commands issued OK - continue?'):
        self.message = message
        self.width = width
        self.wait = wait
        self.command_issued = False
        self.wait_prompt = wait_prompt
        
    def __enter__(self):
        
        if _frame.current_frame:
            return _frame.current_frame
        
        _frame.current_frame = self
        
        if (self.message):
            print '-' * int((self.width - len(self.message) - 2) / 2),
            print self.message,
            print '-' * int((self.width - len(self.message) - 2) / 2)
        else:
            print '-' * self.width
        return self
    
    def __exit__(self, exc_type, exc_val, exc_trace):
    
        if not _frame.current_frame == self:
            return
    
        _frame.current_frame = None
    
        print '-' * self.width
        if self.wait or (self.wait is None and self.command_issued):
            if not console.confirm(self.wait_prompt):
                abort('User aborted command')            
    
    def run(self, command):
        self.command_issued = True
        print command
    
    def sudo(self, command):
        self.run('sudo %s' % command)

