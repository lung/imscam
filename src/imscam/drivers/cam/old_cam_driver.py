import os
from django.conf import settings

class CamDriver(object):
    
    """_max_width = 0
    _max_height = 0
    _exposure_mode_choices = (
            ('0',u'off'),
    )
    _awb_mode_choices = (
            ('0',u'off'),
    )
    _image_dir = os.path.join(settings.MEDIA_ROOT, 'img')
    _time_pattern = '%Y-%m-%d_%H:%M:%S'"""
    
    def __init__(self):
        raise NotImplementedError("could not create base cam driver")
    
    def set_width(self, value):
        raise NotImplementedError("not implemented set_width")
        
    def set_height(self, value):
        raise NotImplementedError("not implemented set_height")
    
    def get_name(self):
        raise NotImplementedError("not implemented get_name")
        
    """def get_shutterspeed(self):
        raise NotImplementedError("not implemented get_shutterspeed")
        
    def get_aperture(self):
        raise NotImplementedError("not implemented get_aperture")
        
    def get_iso(self):
        raise NotImplementedError("not implemented get_iso")
        
    def get_whitebalance(self):
        raise NotImplementedError("not implemented get_whitebalance")"""
    
    def set_shutterspeed(self, value):
        raise NotImplementedError("not implemented set_shutterspeed")
        
    def set_aperture(self, value):
        raise NotImplementedError("not implemented set_aperture")
        
    def set_iso(self, value):
        raise NotImplementedError("not implemented set_iso")
        
    def set_whitebalance(self, value):
        raise NotImplementedError("not implemented set_whitebalance")
    
    def init_cam(self):
        raise NotImplementedError("not implemented init_cam")
            
    def free_cam(self):
        raise NotImplementedError("not implemented free_cam")
    
    def get_settings(self):
        raise NotImplementedError("not implemented get_settings")
    
    def get_preview_settings(self):
        raise NotImplementedError("not implemented get_preview_settings")
    
    def set_setting(self, name, value):
        raise NotImplementedError("not implemented set_setting")
            
    def start_preview(self):
        raise NotImplementedError("not implemented start_preview")
        
    def stop_preview(self):
        raise NotImplementedError("not implemented stop_preview")
    
    def get_image(self, save_to=None):
        raise NotImplementedError("not implemented get_full_res_img")
    
    def get_preview_image(self, size, save_to=None):
        raise NotImplementedError("not implemented get_preview_image")
    
    def get_resized_image(self, size, save_to=None):
        raise NotImplementedError("not implemented get_resized_img")
    
    def get_configured_image(self, settings):
        raise NotImplementedError("not implemented get_configured_image")
        
    def preview_running(self):
        raise NotImplementedError("not implemented preview_running")
    
    """def get_config(self):
        raise NotImplementedError("not implemented")"""