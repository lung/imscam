#from imscam.drivers.cam.cam_driver import CamDriver
from .base import BaseCamDriver, CamDriverError

from django.templatetags.static import static
#from django.conf.urls.static import static
from django.conf import settings

from PIL import Image
import logging

logger = logging.getLogger(__name__)

class CamDriver(BaseCamDriver):
    
    def __init__(self):
        BaseCamDriver.__init__(self)
        self._name = "dummy"
        
        logger.debug("init dummy cam driver")
        self._dummy_image_path = 'static/img/dummy_image.jpg'
    
    def init_cam(self):
        pass
            
    def free_cam(self):
        pass
    
    def get_settings(self):
        return self._settings
    
    def get_preview_settings(self):
        return {}
    
    def start_preview(self):
        self._preview_running = True
        
    def stop_preview(self):
        self._preview_running = False

    def get_preview_image(self, size, save_to=None):
        image = Image.open(self._dummy_image_path).resize( size )
        if save_to:
            image.save(save_to, 'JPEG')
        return image
    
    def get_image(self, save_to=None):
        image = Image.open(self._dummy_image_path)
        if save_to:
            image.save(save_to, 'JPEG')
        return image
    
    def get_resized_image(self, size, save_to=None):
        image = self.get_full_res_img()
        image = image.resize( size )
        if save_to:
            image.save(save_to, 'JPEG')
        return image
    
    def get_configured_image(self, cam_settings):
        try:
            image = self.get_image()
            width = None
            height = None
            for name, setting in cam_settings.items():
                if name == 'width':
                    width = setting
                elif name == 'height':
                    height = setting
            if width and height:
                image = image.resize( (width, height) )
            return image
        except Exception as e:
            logger.error(e)
            raise Exception("Error capturing image for scheduled location")
    
    