"""
Base cam driver class.
"""
#from imscam import settings
#from django.utils.importlib import import_module
import threading
import traceback
import logging

logger = logging.getLogger(__name__)

class CamDriverError(Exception):
    pass


class BaseCamDriver:
    """ Base cam driver class. """
    #CAM_DRIVER = getattr(settings, 'CAM_DRIVER', None)

    def __init__(self):
        self._lock = threading.Lock()
        self._cam = None
        self._cam_keep_open = False
        self._preview_running = False
        
        self._settings = {}
        self._settings['width'] = { 'type':'int', 'current':'',}
        self._settings['height'] = { 'type':'int', 'current':'',}

    def __str__(self):
        return self._name

    @classmethod
    def cam_driver_factory(cls, CAM_DRIVER=None):
        """ Return the correct cam driver object based on the specified CAM_DRIVER. """
        #if not cls.CAM_DRIVER:
        #    raise CamDriverError('You must specify a cam driver class using CAM_DRIVER.')
        try:
            
            if isinstance(CAM_DRIVER, basestring):
                module_path, class_name = CAM_DRIVER.rsplit('.', 1)
                try:
                    module = __import__(module_path, fromlist=[class_name])
                except ImportError, e:
                    raise Exception("Error importing class %s: '%s'" % (CAM_DRIVER, e))
                try:
                    instance = getattr(module, class_name)
                except AttributeError, e:
                    raise Exception("Error importing class %s: '%s'" % (class_name, e))
        
                return instance.CamDriver()
            else:
                raise Exception('Invalid class spec: %s' % CAM_DRIVER)
            
            
            #cam_driver_module = import_module(CAM_DRIVER)
            #driver = cam_driver_module.CamDriver()
            #return driver
        except CamDriverError:
            raise
        except Exception as e:
             logger.error(e)
             raise CamDriverError("cannot create cam driver instance - check settings")
 
    
    def get_name(self):
        return self._name
    
    def preview_running(self):
        return self._preview_running

    ###################################
    #  
    ###################################

    def init_cam(self):
        """
            Initializes the camera.
        """
        raise NotImplementedError("not implemented init_cam")
            
    def free_cam(self):
        """
            Frees the camera.
        """
        raise NotImplementedError("not implemented free_cam")
    
    def get_settings(self):
        """
            Returns settings dictionary for image settings.
        """
        raise NotImplementedError("not implemented get_settings")
    
    def get_preview_settings(self):
        """
            Returns settings dictionary for live preview .
        """
        raise NotImplementedError("not implemented get_preview_settings")
    
    def set_setting(self, name, value):
        """
            Set a camera setting.
        """
        raise NotImplementedError("not implemented set_setting")
            
    def start_preview(self):
        """
            Start live preview. Must set self._preview_running = True
        """
        raise NotImplementedError("not implemented start_preview")
        
    def stop_preview(self):
        """
            Stop live preview. Must set self._preview_running = False
        """
        raise NotImplementedError("not implemented stop_preview")
    
    def get_image(self, save_to=None):
        """
            Returns a full sized image.
        """
        raise NotImplementedError("not implemented get_full_res_img")
    
    def get_preview_image(self, size, save_to=None):
        """
            Returns a preview image.
        """
        raise NotImplementedError("not implemented get_preview_image")
    
    def get_resized_image(self, size, save_to=None):
        """
            Returns a resized image.
        """
        raise NotImplementedError("not implemented get_resized_img")
    
    def get_configured_image(self, settings):
        """
            Returns a configured image by set each setting inside settings dictionary before taking image.
        """
        raise NotImplementedError("not implemented get_configured_image")
