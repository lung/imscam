from django.core.exceptions import MiddlewareNotUsed
from django.conf import settings
from django.core.management import call_command

class StartupMiddleware(object):
    def __init__(self):
        try:
            call_command('migrate', 'imscam')
            call_command('schemamigration', 'imscam', auto=True)
            call_command('migrate', 'imscam')
        except SystemExit as e:
            pass

        raise MiddlewareNotUsed('Startup complete')