import base64
import StringIO
from datetime import datetime
import os
from os import path, listdir
from os.path import isfile, join
from shutil import copyfileobj, copyfile
from PIL import Image
import uuid
import time
import json
import tempfile
from itertools import chain
import logging
from subprocess import Popen, PIPE
import cStringIO

from django.conf import settings as django_settings
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.decorators import permission_required
from django.contrib.sessions.models import Session
from django.contrib.sessions.backends.db import SessionStore
from django.core import serializers
from django.core.management import call_command
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models.fields import AutoField
from django.forms.models import inlineformset_factory
from django.http import HttpResponse, HttpResponseRedirect, StreamingHttpResponse, HttpResponseForbidden
from django.shortcuts import render_to_response, get_object_or_404, render
from django.template.context import RequestContext
from django.template.loader import render_to_string
from django.utils import simplejson
from django.views.decorators.cache import never_cache

from django_sse.views import BaseSseView

from imscam import models, settings, forms
from imscam.settings import CAM_DRIVER, PTZ_DRIVER
from imscam.forms import GroupForm, LocationForm, ImageSettingForm
from imscam.utils import JsonResponse



IMG_ROOT = settings.GROUPS_IMG_ROOT

if not os.path.exists(settings.PREVIEW_TMP_ROOT):
    os.makedirs(settings.PREVIEW_TMP_ROOT) 

logger = logging.getLogger(__name__)


def custom_404(request):
    return render(request,'imscam/404.html') 
def custom_500(request):
    return render(request,'imscam/500.html') 

def login(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                auth_login(request, user)
                next = request.REQUEST.get('next', reverse('imscam:index'))
                logger.debug("next: %s" % next)
                return JsonResponse({'next': next})
            else:
                # Return a 'disabled account' error message
                pass
        else:
            # Return an 'invalid login' error message.
            return HttpResponseForbidden()
    else:
        return render(request, "imscam/login.html", {})

def logout(request):
    auth_logout(request)
    return HttpResponseRedirect(reverse('imscam:index'))

def index(request):
    groups = models.Group.objects.filter(deleted=False)
    group = groups.filter(default=True)[0]
    locations = group.location_set.filter(deleted=False)
    capturetime = models.CaptureTime.objects.get(default=True)
    
    
    sessions = Session.objects.filter(expire_date__gte=datetime.now())
    active_preview_sessions = []
    for session in sessions:
        data = session.get_decoded()
        if data.get('preview-started', False):
            active_preview_sessions.append(session.session_key)
    #print active_preview_sessions
    if request.session.get("preview-started") and len(active_preview_sessions) == 1:
        CAM_DRIVER.stop_preview()
    request.session["preview-started"] = False    
    
    if request.user.has_perm("imscam.change_settings"):
        form = forms.GroupForm(instance=group)
        capturetime_form = forms.CaptureTimeForm(instance=capturetime)
    else:
        form = forms.GroupFormReadOnly(instance=group)
        capturetime_form = forms.CaptureTimeFormReadOnly(instance=capturetime)
    
 
    context = {'language': django_settings.LANGUAGE_CODE,
               'form': form, 
               'capturetime_form': capturetime_form,
               'location_list': locations,
                    'groups': groups,
                   'group': group,
                   'default_capturetime':capturetime,
                   'preview_settings': CAM_DRIVER.get_preview_settings(),
                   'MEDIA_PREVIEW': settings.MEDIA_PREVIEW}
    context['group_id'] = group.id
    context['PTZ_DRIVER'] = PTZ_DRIVER
    unsaved_locations = request.session.get("new-locations_%s" % group.id)
    
    logger.debug("unsaved_locations from index")
    logger.debug(unsaved_locations)
    #print unsaved_locations        
    if unsaved_locations:
        for session_loc in unsaved_locations:
        #for random in unsaved_locations:
            logger.debug("loading unsaved location from session")
            loc = models.Location()
            loc.random = session_loc[0]
            loc.tmp_pan = session_loc[1][0]
            loc.tmp_tilt = session_loc[1][1]
            context['location_list'] = list( chain( context['location_list'], [loc] ) )
        
    return render(request, 'imscam/index.html', context)


class SsePreviewEvents(BaseSseView):
    def iterator(self):
        #print self.request
        interval = self.request.GET.get('interval')
        interval_sec = float(interval) / 1000
        if CAM_DRIVER.get_name() == 'dummy' and interval_sec == 0:
            interval_sec = 0.5
        while True:
            try:
                preview_running = CAM_DRIVER.preview_running()
                if not preview_running:
                    self.sse.add_message("preview-stopped", "live preview has been stopped")
                    break
                img = CAM_DRIVER.get_preview_image((600,400))
                if img:
                    output = StringIO.StringIO()
                    img.save(output, format="JPEG")
                    contents = output.getvalue()
                    output.close()
                    b64_img = base64.b64encode(contents)
                    
                    self.sse.add_message("preview-img", b64_img)
                time.sleep(interval_sec)
            except Exception, e:
                logger.error("error in SsePreviewEvents")
                logger.error(e)
                self.sse.add_message("error", "live preview has been stopped")
                break
            finally:
                yield
        
            


@never_cache
@permission_required('imscam.view_liveimage')
def get_preview_img(request):
    
    """started = False
    for s in Session.objects.all():
        if s.get_decoded().get("preview_started"):
            started = True"""
            
    #preview_state = models.LivePreviewState.objects.all()[0]
    preview_running = CAM_DRIVER.preview_running()
    
    #if not preview_state.running:
    if not preview_running:
        return HttpResponse("live preview has been stopped", status=410)
    
    try:
        #img = CAM_DRIVER.get_resized_image_PIL(600, 400)
        img = CAM_DRIVER.get_preview_image((600,400))
        if not img:
            return HttpResponse()
        output = StringIO.StringIO()
        img.save(output, format="JPEG")
        contents = output.getvalue()
        output.close()
        
        b64_img = base64.b64encode(contents)
        return HttpResponse(b64_img)
    except Exception, e:
        #preview_state.running = False
        #preview_state.save()
        return HttpResponse(str(e), status=410)

  
    
@permission_required('imscam.view_liveimage')    
def start_preview(request):
    if request.method == 'POST':
        
        #preview_state = models.LivePreviewState.objects.all()[0]
        preview_running = CAM_DRIVER.preview_running()
        
        #if preview_state.running:
        if preview_running:
            logger.debug('preview already started')
            return JsonResponse({'success':True, 'msg': 'preview already started'})
        
        if PTZ_DRIVER:
            PTZ_DRIVER.reset()
        
        try:
            CAM_DRIVER.start_preview()
            request.session["preview-started"] = True
        except Exception as e:
            return JsonResponse({'msg':e.message})
        
        return JsonResponse({'success':True, 'msg':'started preview', 'pan':PTZ_DRIVER.getPan(), 'tilt':PTZ_DRIVER.getTilt()})
        #return HttpResponse('started preview')
    return HttpResponse('start_preview with GET')

@permission_required('imscam.view_liveimage')    
def stop_preview(request):
    if request.method == 'POST':
        #print "enter stop_preview()"
        #preview_state = models.LivePreviewState.objects.all()[0]
        preview_running = CAM_DRIVER.preview_running()
    
        if not preview_running:
            return HttpResponse("already stopped preview")
        
        try:
            #print "capture image"
            img = CAM_DRIVER.get_preview_image((600, 400))
            img.save(settings.PREVIEW_FULL_PATH)
        except Exception as e:
            print "exception capture"
            print e
        try:
            
            sessions = Session.objects.filter(expire_date__gte=datetime.now())
            active_preview_sessions = []
            for session in sessions:
                data = session.get_decoded()
                if data.get('preview-started', False):
                    active_preview_sessions.append(session.session_key)
            #print active_preview_sessions
            if request.session.get("preview-started") and len(active_preview_sessions) == 1:
                #print "delegate stop_preview"
                CAM_DRIVER.stop_preview()
                #if PTZ_DRIVER:
                #    print "resetting PTZ_DRIVER"
                #    PTZ_DRIVER.reset()
            request.session["preview-started"] = False
        except Exception as e:
            print e    
        
        return HttpResponse('stopped preview')
    return HttpResponse('stop_preview with GET') 

def copy_preview(request):
    if request.method == "POST":
        group_id = request.POST.get("group_id")
        location = models.Location()
        
        
        logger.debug("preparing copy preview group: %s" % group_id)
        random = uuid.uuid4()
        location.random = str(random)
        
        
        
        save_to = settings.PREVIEW_TMP_ROOT + "/%s.jpg" % random
        logger.debug("dst: %s" % save_to)
        
        pan = None
        tilt = None
        try:
            img = CAM_DRIVER.get_preview_image( (100,80) )
            if not img:
                return HttpResponse("could not create location from preview", status=410)
            img.save(save_to)
            if PTZ_DRIVER:
                pan = PTZ_DRIVER.getPan()
                tilt = PTZ_DRIVER.getTilt()
                location.tmp_pan = pan
                location.tmp_tilt = tilt
                
            add_to_session = [ str(random), [pan, tilt] ]
            unsaved_locations = request.session.get("new-locations_%s" % group_id)
            logger.debug("unsaved locations...")
            logger.debug(unsaved_locations)
            if unsaved_locations:
                unsaved_locations.append( add_to_session )
            else:
                unsaved_locations = [ add_to_session ]
            request.session["new-locations_%s" % group_id] = unsaved_locations
            
            logger.debug("session:")
            logger.debug(request.session["new-locations_%s" % group_id])
            
            #url = "%simg/preview/tmp/%s.jpg" % (settings.MEDIA_URL, random)
            return render(request, 'imscam/include/locations/location.html', {"location": location, "group_id": group_id, "PTZ_DRIVER": PTZ_DRIVER})
        except Exception, e:
            logger.debug(e)
            return HttpResponse(str(e), status=410)
    
    
    return HttpResponse("copy_preview call with GET")



  
@permission_required('imscam.change_settings')  
def play_pause(request):
    if request.method == "POST":
        id = request.POST.get("id")
        if not id:
            return HttpResponse("error")
        
        capturetime = models.CaptureTime.objects.get(id=id)
        active = True
        if request.POST.get("active") == "false":
            active = False
        
        logger.debug("changing capturetime id=%s, new state = %s" % (id, active))
        
        if active:
            capturetime.enabled = True
        else:
            capturetime.enabled = False
        capturetime.save()
            
        return HttpResponse("play_pause success")
    return HttpResponse("play_pause call with GET")

@permission_required('imscam.change_settings')    
def cam_settings(request):
    default_capturetime = models.CaptureTime.objects.get(default=True)
    capturetime_form = forms.DefaultCaptureTimeForm(request.POST or None, instance=default_capturetime)
    
    if request.method == "POST":
        #if form.is_valid() and capturetime_form.is_valid():
        if capturetime_form.is_valid():
            #form.save()
            capturetime = capturetime_form.save()
            
            if request.POST.get("overwrite_group_locations"):
                update_existing_groups(capturetime)
            
            return HttpResponse(json.dumps({'status':'success', 'msg':'default settings saved',}), mimetype='application/json')
            #return HttpResponseRedirect(reverse('imscam:cam_settings'))
        
        
        data = json.dumps({'errors': dict([(k, [unicode(e) for e in v]) for k,v in capturetime_form.errors.items()])})
        return HttpResponse(data, mimetype='application/json')
    
    context = {'capturetime_form':capturetime_form, 
                   #'form': form,
                   'default_setting': True,}
    return render(request, 'imscam/settings.html', context)

@permission_required('imscam.view_liveimage')
def set_config(request):
    if request.method == "POST":
        name = request.POST.get("name")
        value = request.POST.get("value")
        CAM_DRIVER.set_setting(name, value)
    return JsonResponse({'success':True, 'msg':'Successfully changed preview settings'})

def update_existing_groups(capturetime):
    groups = models.Group.objects.not_deleted()
    for group in groups:
        copy_fields_and_save(capturetime, group)
        update_existing_locations(capturetime, group)
            
def update_existing_locations(capturetime, group):
    for location in group.location_set.not_deleted():
        copy_fields_and_save(capturetime, location)

def copy_fields_and_save(capturetime, instance):
    i_capturetime = instance.capture_time
    for field in capturetime._meta.get_all_field_names():
        logger.debug(field)
        if field == "group" or field == "id" or field == "location" or field == "default":
            continue
        
        value = getattr(capturetime, field)
        logger.debug(value)
        setattr(i_capturetime, field, value)
    i_capturetime.save()
    
    
    
def groups(request):
    groups = models.Group.objects.not_deleted()
    data = serializers.serialize('json', groups, fields=('name'))
    return JsonResponse(data)

def group_edit(request, group_id=None):
    if group_id:
        # edit group
        group = get_object_or_404(models.Group, pk=group_id)
        capturetime = group.capture_time
    else:
        # new group
        group = models.Group()
        capturetime = models.CaptureTime.objects.get(default=True)
    
    if request.user.has_perm("imscam.change_settings"):
        form = forms.GroupForm(request.POST or None, instance=group)
        capturetime_form = forms.CaptureTimeForm(request.POST or None, instance=capturetime)
    else:
        form = forms.GroupFormReadOnly(request.POST or None, instance=group)
        capturetime_form = forms.CaptureTimeFormReadOnly(request.POST or None, instance=capturetime)
    
    
    if request.method == "POST":
        
        if form.is_valid() and capturetime_form.is_valid():
            capturetime = capturetime_form.save(commit=False)
            group = form.save(commit=False)
            
            if group_id:
                capturetime.save()
            else:
                
                # save new group
                capturetime.pk = None
                capturetime.default = False
                capturetime.save()
                
                group.capture_time = capturetime
            
            if request.POST.get("overwrite_group_locations"):
                update_existing_locations(capturetime, group)
                
            group.save()
            
            html = render_to_string( 'imscam/include/groups/group_item.html', {'group': group, 'perms': request.user.get_all_permissions()} )
            res = {'html': html, 'status': 'success'}
            return HttpResponse( simplejson.dumps(res), 'application/json' )
        
        error_forms = {'form_errors': form.errors, 'capturetime_form_errors': capturetime_form.errors,}
        context = {'id': group.id, 'error_forms': error_forms}
        return JsonResponse(context)
    else:
        context = {'form': form,
                   'capturetime_form': capturetime_form, 
                   'group': group,}
        return render(request, 'imscam/include/settings/group_location_form.html', context)
    

def group_delete(request, group_id):
    group = get_object_or_404(models.Group, pk=group_id)
    if group.default:
        return JsonResponse({'status': 'error', 'msg': 'not allowed to delete default group'})
    group.delete()
    return JsonResponse({})







def locations(request, group_id):
    group = get_object_or_404(models.Group, pk=group_id)
    locations = group.location_set.not_deleted()
    #action = request.GET.get("action")
    unsaved_locations = request.session.get("new-locations_%s" % group_id)        
    if unsaved_locations:
        #for random in unsaved_locations:
        for session_loc in unsaved_locations:
            logger.debug("loading location from session")
            loc = models.Location()
            loc.random = session_loc[0]
            loc.tmp_pan = session_loc[1][0]
            loc.tmp_tilt = session_loc[1][1]
            locations = list( chain( locations, [loc] ) )
    
    
    return render(request, 'imscam/include/locations/location_list.html', {'location_list': locations, 'group_id': group_id, 'PTZ_DRIVER': PTZ_DRIVER})

def location_edit(request, group_id, location_id=None):
    group = get_object_or_404(models.Group, pk=group_id)
    
    if location_id:
        new = False
        logger.debug("location_id=%s" % str(location_id))
        location = get_object_or_404(models.Location, pk=location_id)
        img_settings = location.image_settings
        capturetime = location.capture_time
    else:
        logger.debug("no location id")
        
        new = True
        location = models.Location()
        location.driver_name = CAM_DRIVER.get_name()
        
        location.pan = request.GET.get("pan")
        location.tilt = request.GET.get("tilt")
        location.group = group
        
        s = CAM_DRIVER.get_settings()
        img_settings = models.ImageSetting()
        for field in img_settings._meta.fields:
            if type(field) == AutoField :
                continue
            setattr(img_settings, field.name, s[field.name]['current'])
        capturetime = group.capture_time
    
        
    #location, created = models.Location.objects.get_or_create(defaults={})
    
    if request.user.has_perm("imscam.change_settings"):
        form = forms.LocationForm(request.POST or None, instance=location)
        image_settings_form = forms.ImageSettingForm(request.POST or None, instance=img_settings)
        capturetime_form = forms.CaptureTimeForm(request.POST or None, instance=capturetime)
    else:
        form = forms.LocationFormReadOnly(request.POST or None, instance=location)
        image_settings_form = forms.ImageSettingFormReadOnly(request.POST or None, instance=img_settings)
        capturetime_form = forms.CaptureTimeFormReadOnly(request.POST or None, instance=capturetime)
    
    
    if request.POST:
        if form.is_valid() and capturetime_form.is_valid() and image_settings_form.is_valid():
            capturetime = capturetime_form.save(commit=False)
            img_settings = image_settings_form.save()
            location = form.save(commit=False)
            location.image_settings = img_settings
            
            if location_id:
                capturetime.save()
            else:
                logger.debug(request.POST.get("random"))
                location.tmp_preview = "%s/img/preview/tmp/%s.jpg" % (settings.MEDIA_ROOT, request.POST.get("random"))
                capturetime.pk = None
                capturetime.save()
                
                location.capture_time = capturetime
            logger.debug("location group before save %s" % location.group)    
            location.save()
            
            if new:
                unsaved_locations = request.session["new-locations_%s" % group.id]
                random_param = str(request.POST.get("random"))
                for item in list(unsaved_locations):
                    if item[0] == random_param:
                        unsaved_locations.remove(item)
                request.session["new-locations_%s" % group.id] = unsaved_locations
            
            html = render_to_string( 'imscam/include/locations/location.html', {'location': location, 'group_id': group_id, 'perms': request.user.get_all_permissions(), 'PTZ_DRIVER': PTZ_DRIVER} )
            data = {'html': html, 'status': 'success'}
            return HttpResponse( simplejson.dumps(data), 'application/json' )
            
        else:
            logger.error("location form not valid")
            #return HttpResponse(status=422)
            error_forms = {'location_form_errors': form.errors,
                           'capturetime_form_errors': capturetime_form.errors,
                           'image_settings_form_errors': image_settings_form.errors}
            context = {'id': location.id,
                       'error_forms': error_forms}
            return JsonResponse(context)
        
    context = {'form': form, 
               'image_settings_form': image_settings_form,
               'capturetime_form': capturetime_form,
               'location': location,
               'group_id': group_id, }
    return render(request, 'imscam/include/settings/group_location_form.html', context)


def location_update_pan_tilt(request):
    location_id = request.POST.get("id")
    #print location_id
    if not location_id:
        return HttpResponse("successfully updated tmp location pan tilt values")
    
    pan = request.POST.get("pan")
    tilt = request.POST.get("tilt")
    location = get_object_or_404(models.Location, pk=location_id)
    location.pan = pan
    location.tilt = tilt
    location.save()
    
    return HttpResponse("successfully updated pan tilt values")


def location_delete(request, location_id):
    try:
        get_object_or_404(models.Location, pk=location_id).delete()
        return JsonResponse({'success':True, 'msg': 'successfully removed location'})
    except Exception as e:
        logger.error(e)
        return JsonResponse({'msg':e})
    

def tmp_location_delete(request, group_id):
    if request.method == "POST":
        random = request.POST.get("random")
        if not random:
            return JsonResponse({'msg': 'no random-var in post data'})
        
        unsaved_locations = request.session["new-locations_%s" % group_id]
        i = 0
        pos = -1
        for x in unsaved_locations:
            if x[0] == str(random):
                pos = i
                break
            i = i + 1
            
        if pos == -1:
            return JsonResponse({'success': True, 'msg': 'removed tmp location - location not in session'})
        
        unsaved_locations.pop(pos)
        #unsaved_locations.remove( str(random) )
        request.session["new-locations_%s" % group_id] = unsaved_locations
        
        try:
            os.remove( "%s/%s.jpg" % (settings.PREVIEW_TMP_ROOT, random) )
        except:
            return JsonResponse({'success': True, 'msg': 'tmp location removed but no tmp image found'})
        
        return JsonResponse({'success': True, 'msg': 'removed tmp location'})
    else:
        return JsonResponse({'msg': 'no POST request'})


@permission_required('imscam.view_recordings')
def browse_images(request):
    groups = models.Group.objects.not_deleted()
    
    return render_to_response('imscam/gallery.html', {'groups': groups}, context_instance=RequestContext(request))


def get_files(request, location_id, page=None):
    logger.info("loading imagefiles for location: %s" % str(location_id))
    location = get_object_or_404(models.Location, pk=location_id)
    
    location_images_list = models.LocationImage.objects.all().filter(location=location).order_by('-time')
    paginator = Paginator(location_images_list, 9)
    
    #page = request.GET.get('page')
    try:
        location_images = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        location_images = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        location_images = paginator.page(paginator.num_pages)
    
    return render(request, "imscam/include/gallery/images.html", {"location_id": location_id, "location_images": location_images})

def get_latest_locationimage(request, locationimage_id):
    logger.info("loading lates locationimage")
    locationimage = get_object_or_404(models.LocationImage, id=locationimage_id)
    
    start = time.time()
    img = Image.open(locationimage.image.path)
    img = img.resize((100, 60))
    logger.debug(time.time() - start)
    
    response = HttpResponse(mimetype="image/jpeg")
    img.save(response, "JPEG")
    return response



def help(request):
    return render_to_response('imscam/help.html', {}, context_instance=RequestContext(request))

def backup(request):
    if request.method == 'POST':
        if 'database' in request.POST:
            call_command('dbbackup', compress=True)
            data = {'success': True, 'msg': 'Successfully uploaded database to the FTP-Server'}
            #print "returning json..."
            return HttpResponse(json.dumps(data), mimetype='application/json')
        elif 'media' in request.POST:
            call_command('backup_media')
            return JsonResponse({'success':True, 'msg':'Successfully uploaded media files to the FTP-Server'})
        elif 'reset' in request.POST:
            # TODO: remove migration files?
            models.Group.objects.not_deleted().delete()
            models.Location.objects.not_deleted().delete()
            models.Location.objects.all().delete()
            models.CaptureTime.objects.all().delete()
            models.ImageSetting.objects.all().delete()
            models.LocationImage.objects.all().delete()
            call_command('loaddata', 'default_entries')
            return JsonResponse({'success':True, 'msg':'Successfully resetted database'})
            
    return render_to_response('imscam/backup.html', {}, context_instance=RequestContext(request))