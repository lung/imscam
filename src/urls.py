from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import RedirectView
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

#import ptz_control
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

handler404 = 'imscam.views.custom_404'
handler500 = 'imscam.views.custom_500'

urlpatterns = patterns('',
    # Examples:
    url(r'^$', RedirectView.as_view(url='/imscam/')),
    
    url(r'^imscam/', include('imscam.urls', namespace='imscam')),


    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    
    #url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
     #   'document_root': settings.MEDIA_ROOT,
    #}),
)
#urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()