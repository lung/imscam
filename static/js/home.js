

function init() {
	
	$("#groups button.edit").hide();
    $("#groups .delete").hide();
    
    $('.ptz_move').attr('disabled', true);
    $('#ptz_goto').attr('disabled', true);
    $("#preview-img").css({ opacity: 0.5 });
    
    
    $('a.group-link.default').addClass("active");
	$('a.group-link.default').find("button").show();
    
}










$(function() {
	$(".tooltip-elem").tooltip();
	
	
	activateDatePicker();
    $("#group-form .capture-time-settings").hide();
    
    $("#group-form .capture-time-settings-select").change();
    /*$("#group-location-form-panel #form-errors").hide();*/
    

    
    
    $(document).on('click', 'a.group-link', function(event) {
		
		if ( $(event.target).is("button") ) {
			return false;
		}
		
		activateGroup($(this));

	    return false;
	});


	$("#group-new").click(function() {
	    var url = $(this).data("url");
	    showGroupSettings(url);
	});

	
	$(document).on("click", "#groups .edit", function() {
	//$("#groups button.edit").click(function() {
	    console.log("group-edit");
	    var group_id = $('#groups a.active').data("id");
	    console.log(group_id);
	    var url = $(this).data("url");
	    showGroupSettings(url);
	});

	$(document).on("click", "#groups button.delete", function(event) {
	//$("#groups button.delete").click(function(event) {
		/*var target = $(this).closest(".list-group-item");
	    var group_name = $('a.group-link.active .list-group-item-heading').text(); 
	    var url = $(this).data("url");*/
	    
	    
	    event.preventDefault();
		var group = $(this).closest(".list-group-item");
		deleteGroup(group);
		return true;
	    
	});

	$(document).on("click", "#group-form-submit", function() {
	    //$("#group-form-submit").click(function() {
	    	var form = $(this).closest("#group-location-form-panel").find("form");
	    	var url = $(this).data("url");
	    	var data =  form.serialize();
	    	var id = form.find("input[name='id']").val();
	    	console.log("id: " + id);
	    	
	    	form.find("input[disabled]").each( function() {
	            data = data + '&' + $(this).attr('name') + '=' + $(this).val();
	        });
	    	console.log(data);
	        $.ajax({
	            type: "POST",
	            url: url,
	            dataType: 'json',
	            data: data,
	            
	            success: function(response) {
	            	if (response.status != 'success') {
		        		console.log("group save error");
		        		$("#group-location-form-panel #form-errors").empty();
		        		
		        		$.each(response.error_forms, function(key, val) {
		        			$.each(response.error_forms[key], function(k, v) {
		                        console.log(k + ":" + v);
		                        $("#group-location-form #form-errors").append("<div><strong>"+k+":</strong> " + v + "</div>");
		                    });
		                });
		        		$("#group-location-form #form-errors").fadeIn( "slow" );
		        		activateDatePicker();
		        		/*scrollTo("#group-location-form-panel");*/
		        		return false;
		
		                
		        		
		        		
		        	}
		        	else {
		        		console.log("group saved");
		        		
		        		displayAlert("success", "group saved");
		        		if (id == "None") {
		        			console.log("add new group");		                	
		                	$("#groups .list-group").append(response.html);
		                	$("#groups .list-group a:last").click();
		        		} else {
		        			$("#groups a.active").html($(response.html).html());
		        		}
		        		
		        		if (response.html.indexOf("overwrite_group_locations") >= 0) {
		        			console.log("overwrite");
		        			var reloadUrl = $(".group-link.active").attr("href");
		        			console.log("reload url " + reloadUrl);
		        			$("#group-locations").load(reloadUrl);
		        		}
		        		
	                	return true;
		            	
		            	
		        	}
	        
	                
	                
	            },
	            error: function(xhr, status, error) {
	            	console.log("group validation error");
	            	form.html( $(xhr.responseText).find("form").html() );
	            	activateDatePicker();
	            }
	        });
	    });
	

	
	
	
	
	
	
	
	
	$(document).on('click', 'li.group-location', function(event) {
	    
	    if( $(event.target).is("button") || $(event.target).is('[class^="onoffswitch"]')) {
	        return true;
	    }
	    
	    activateLocation($(this));
	});
	
	
	
	
	$(document).on('click', '#location-form-submit', function(event) {
		/*var id = $("form#location-form input[name='id']").val();
		console.log(id);*/
		/*var group_id = $('select#group-select option:selected').val();*/
		event.preventDefault();
		var group_id = $('a.group-link.active').data("id");
		var wp_id = $("#group-location-form").find("input[name='id']").val();
		var url = $(this).data("url");
		var data = $('#group-location-form').serialize();
		var selected = $(".group-location.active");
		console.log(selected);
		console.log(wp_id);
		
		if (wp_id == "None") {
			data = data + "&random=" + selected.find("input[name='random']").val();
			data = data + "&pan=" + selected.find("input[name='pan']").val();
			data = data + "&tilt=" + selected.find("input[name='tilt']").val();
		}
	    $.ajax({
	        type: "POST",
	        url: url,
	        data: data,
	        dataType: 'json',
	        success: function(data) {
	        	if (data.status != 'success') {
	        		console.log("wp save error");
	        		$("#group-location-form-panel #form-errors").empty();
	        		
	        		$.each(data.error_forms, function(key, val) {
	        			$.each(data.error_forms[key], function(k, v) {
	                        console.log(k + ":" + v);
	                        $("#group-location-form #form-errors").append("<div><strong>"+k+":</strong> " + v + "</div>");
	                    });
	                });
	        		$("#group-location-form #form-errors").fadeIn( "slow" );
	        		
	        		/*scrollTo("#group-location-form-panel");*/
	        		return false;
	
	                
	        		
	        		
	        	}
	        	else {
	        		console.log("wp saved");
	        		var li = $(data.html);
	        		selected.replaceWith(li);
	        		
	        		$("#wp-form #form-errors").empty();
	        		$("#group-location-form-panel #form-errors").fadeOut( "slow" );
	        		highlightElem(li);
	        		li.click();
	        	    /*var originalColor = selected.css("background");
	            	selected.css("background", "#93FF8A");
	            	setTimeout(function(){
	            	    selected.css("background", originalColor);
	            	}, 2500);*/
	        		
	                /*scrollTo("#main");*/
	        		
	                displayAlert("success", "location saved");
	            	return false;
	            	
	            	
	        	}
	            
	        },
	        error: function(xhr, status, error) {
	        	console.log("wp save error...");
	        	//$('#wp-form').html( $(xhr.responseText).find("#wp-form").html() );
	        }
	    });
		
		return false;
	});
	
	
	$(document).on('click', 'li.group-location .close', function(event) {
		event.preventDefault();
		var $location = $(this).closest("li.group-location");
		deleteLocation($location)
		return true;
	});
	
	
	$(document).on('click', '#form-close', function(event) {
		$('#group-location-form-panel').hide();
	    $(".group-location.active").removeClass("active");
	});
	
	
	
	
	
	
	

	
	
	
	$(document).on("click", ".scheduler-enabler", function(event) {
    	if ( $(this).is(":disabled") ) {
    		return false;
    	}
        event.preventDefault();
        
        var form = $(this).closest("form");
        var id = form.find("input[name='id']").val();
        var enabled = $(this).hasClass("active");
        console.log(id + " " + enabled);
        
        $.ajax({                                                                                                                           
    	    type: "POST",
    	    url: form.attr("action"),                                                                                                    
    	    data: {id:id, active:enabled}
    	}).done(function(response) {
    	}).fail(ajaxFailCallback);
        return false;     
    });
	
	
	
	$(document).on('click', '.settings-panel a', function (e) {
        e.preventDefault();
        $(this).tab('show');
    });
	
	
	
	
	
	
	
	//$('a.group-link.default').click();
	
	/*$(document).on('hover', 'li.group-location', function(event) {
		$(this).find(".close").toggle();
		return true;
	});*/
	
	

	
	

	/*$(document).on('click', 'li.group-location button.delete', function(event) {
		var $location = $(this).closest("li.group-location");
		deleteLocation($location)
	});*/
	
	
	
	
	
	
	/*$("#wp-form #form-errors").hide();
	$("#wp-form #cam-settings").hide();
	$("#wp-form .capture-time-settings").hide();
	$("#wp-form .capture-time-settings-select").change();*/
	
	
	
	
	$('#capture-settings-select').change(function() {
	    var disable = false;
	    if ($("#capture-settings-select option:selected").attr("value") == "default") {
	        disable = true;
	    }
	    $("#capture-settings input[type='text']").each(function() {
	        $(this).prop('disabled', disable);
	        //alert($(this).val());
	    });
	}).change();
	
	
	
	$("input#show-cam-settings").change(function() {
		
		if( $(this).is(':checked')){
			$("#cam-settings").show();
		} else {
			$("#cam-settings").hide();
		}
	});
	
	
	
	
	
	
	
	$(document).on('click', '.collapse-section', function (event) {
		var target = $(event.target);
		target.closest(".panel").find(".panel-body").toggle();
		target.closest(".panel").find(".panel-footer").toggle();
		
		var active_group_name = $(".list-group-item.active").find(".list-group-item-heading").html();
		
		
		if ( target.closest("section").attr("id") == "groups-section" ) {
			console.log(active_group_name);
			$("#location-section").find("#active-group").toggle();
			$("#location-section").find("#active-group #active-group-name").html(active_group_name);
		}
		
	    var span = target.find("span");
	    if (span.hasClass("glyphicon-chevron-down")) {
	        span.removeClass("glyphicon-chevron-down");
	        span.addClass("glyphicon-chevron-right");
	    } else {
	        span.removeClass("glyphicon-chevron-right");
	        span.addClass("glyphicon-chevron-down");
	    }
	});
	
	
	
	
	
	
	
	
	



});



function highlightElem(elem) {
	var original_color = elem.css("background");
	
    elem.css("background", "#F5F5F5");
    setTimeout(function() {
    	elem.css("background", original_color);
    }, 500);
}


var deleteGroup = function(group) {
    var group_name = group.find('.list-group-item-heading').text(); 
    var url = group.find(".delete").data("url");
	
	bootbox.dialog({
  	  message: "Are you sure you want to delete group '" + group_name + "' ?",
  	  title: "Confirmation ",
  	  buttons: {
  	    confirm: {
  	      label: "Delete",
  	      className: "btn-danger",
  	      callback: function() {	
		          $.ajax({
		    	        type: "POST",
		    	        url: url,
		    	        success: function(data) {
		    	            console.log("group deleted");
		    	            
		    	            group.hide('slow', function(){ group.remove(); });
		    	            
		    	            $("#group-locations").html("");
		    	            //disableWpsEditMode();
		    	            displayAlert("success", "group removed");
		    	            $("#groups .list-group a:first").click();
		    	        }
		    	    });
  	      }
  	    },
  	    abort: {
  	      label: "Abort",
  	      className: "btn-default",
  	      callback: function() {
  	      }
  	    }
  	  }
  	});
}


var saveGroup = function(group) {
	
}


function deleteLocation($location) {
	var id = $location.find("input[name='id']").val();
	var url = $location.find(".close").data("url");
	var wp_name = $location.find("span.location-name").text();
	var img = $($location).find("img")[0].outerHTML;
	
	/*if ( !id ) {
		$location.remove();
		return true;
	}*/
    
    console.log("'" + wp_name + "'");
    
    /*var answer = confirm("Are you sure you want to delete location '" + wp_name + "'?");
    if (!answer) {
        return false;
    }*/    
    /*bootbox.confirm("Are you sure you want to delete location '" + wp_name + "'?" + "<img></img>", function(result) {
    	answer = result;
    });*/
    
    
    console.log($location);
    console.log(img);
    
    
    
    bootbox.dialog({
  	    message: "Are you sure you want to delete location '" + wp_name + "' ? <br />" + img,
  	    title: "Confirmation",
  	    buttons: {
  	        danger: {
  	            label: "Delete",
  	            className: "btn-danger",
  	            callback: function() {
  	            	console.log("removing location");
  	            	if (id != "" && id != "None" && id != "0") {
  	            		$.post(url, function(response) {
  	            			if (response.success) {
  	            				console.log("remove now from list");
  	            				removeLocationFromList($location);
  	  	    	            	displayAlert("success", response.msg);
  	            			} else {
  	  	    	            	displayAlert("danger", response.msg);
  	            			}
  	            		});
  	            	} else {
  	            		/* remove random from session */
  	            		var group_id = $(".group-link.active").data("id");
  	            		var random = $location.find("input[name='random']").val();
  	            		console.log("randon:"+random);
  	            		$.post(url, {random: random}, function(response) {
  	            			if (response.success) {
  	            				console.log("remove now from list");
  	            				removeLocationFromList($location);
  	  	    	            	displayAlert("success", response.msg);
  	            			} else {
  	  	    	            	displayAlert("danger", response.msg);
  	            			}
  	            		});
  	            	}
  	            }
  	        },
  	        main: {
  	        	label: "Abort",
  	        	className: "btn-default",
  	        	callback: function() {}
  	        }
        }
  	});
}


var removeLocationFromList = function(location) {
	location.hide('slow', function(){ 
		location.remove();
		if ( $("#group-locations li").length == 0 ) {
			$("#group-locations").html("<p>No locations saved</p>");
		}
	});
}


function activateDatePicker() {
	$('.bootstrap-datetimepicker-widget.dropdown-menu').remove();
	$('.start-date').datetimepicker({
		language: language,
		pickTime: false,
	}).on("change.dp",function (e) {
	    var end = $(this).closest("form").find(".end-date");   		
	    end.data("DateTimePicker").setStartDate(e.date);
	});

	$('.end-date').datetimepicker({
		language: language,
		pickTime: false,
	}).on("change.dp",function (e) {
		var start = $(this).closest("form").find(".start-date");
	    start.data("DateTimePicker").setEndDate(e.date);
	});
}





var activateGroup = function(group) {
    
    $.ajax({
        type: "GET",
        url: group.attr("href")
    }).done(function(response) {
    	$('#group-locations').html(response);
        $('#group-locations').find(".tooltip-elem").tooltip();
        
	    var group_id = group.data("id");
	    var url = group.find(".edit").val();
	    showGroupSettings(url, function() {
	    	$('a.group-link.active').find(".delete").hide();
	    	$('a.group-link.active').removeClass("active");
	        group.find(".delete").show();
	        group.addClass("active");
	    });
    }).fail(ajaxFailCallback);
        
};

var activateLocation = function(location, callback) {
    var id = location.find("input[name='id']").val();
    var group_id = $('a.group-link.active').data('id');
    var url = location.data("url");
    var data = "";
    
    if (id == "None") {
    	var pan = location.find("input[name='pan']").val();
    	var tilt = location.find("input[name='tilt']").val();
    	data = "pan="+pan+"&tilt="+tilt;
    }
    
    showLocationSettings(url, data, function() {
    	$(".group-location.active").removeClass("active");
        location.addClass("active");
        if (callback && typeof(callback) === 'function') {
        	callback();
        }
    });
};

var showGroupSettings = function(url, callback) {
	$.ajax({
        type: 'GET',
        url: url
	}).done(function(response) {
		$("#group-location-settings").html(response);
		$('.group-location.active').removeClass("active");
    	activateDatePicker();
    	highlightElem($('#group-location-form-panel'));
    	if (callback && typeof(callback) === 'function') {  
    		callback();
    	}
	}).fail(ajaxFailCallback);
};

var showLocationSettings = function(url, data, callback) {
	$.ajax({
        type: 'GET',
        url: url,
        data: data
	}).done(function(response) {
		$("#group-location-settings").html(response);
		activateDatePicker();
    	highlightElem($('#group-location-form-panel'));
    	if (callback && typeof(callback) === 'function') {  
    		callback();
    	}
	}).fail(ajaxFailCallback);
};