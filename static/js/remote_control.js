var previewStarted = false;
var previewRunning = false;
var timerId = 0;
var oldTimerVal = 0;
var previewUpdateTimeout = 0;
var previewUpdateTimeoutIds = new Array();
var updateStarted = false;
var updatePreviewAjax;
var updatePreviewTime;
var stopButtonPressed = false;
var change_preview_setting_interrupt = false;
var sse_preview_source;

var previewMode = "";

$(function() {
	$(document).on('click', '#start-preview-btn', function(event) {
		console.log("start button click");
    	//console.log(timerId);
    	startPreview();
    });
	
	$(document).on('click', '.start-preview', function(event) {
		startPreview();
	});
	
	$(document).on('click', 'li.group-location .move', function(event) {
		event.preventDefault();
		
		
		/*if (pan == "None" || tilt == "None") {
			pan = location.find("input[name='tmp_pan']").val();
			tilt = location.find("input[name='tmp_tilt']").val();
		}*/
		
		
		var location = $(this).closest('.group-location');
		var success = startPreview(location);
		//var success = startPreview(this, ptz_post_move_call);
		//var success = startPreview();
		console.log(success);
		if (success) {
			var location = $(this).closest('li.group-location');
			//ptz_move_to_location(location);
			
		}
		
	});
	
	
	$(document).on('click', '.stop-preview', function(event) {
    	console.log("stop button click");
    	stopButtonPressed = true;
    	$("#spinner").show();
    	$(this).prop('disabled', true);
    	$(".stop-preview").prop('disabled', true);
    	$(".move").prop('disabled', true);
    	
    	/*if (previewMode == "polling") {
    		
    	}*/
    		stopPreview();
    	//stopPreview();
    	/*$("#viewing-location").hide();*/
    	
    });
	
	$('#preview_update_interval').on('change keypress paste focus textInput input', function () {
	    var val = this.value;
	    
	    if (isNaN(val) || val.length <= 0 || val <= 0) {
	    	$(this).val("");
	    	val = 0;
	    }
	    
	    if (oldTimerVal != val) {
	    	oldTimerVal = val;
	    	
	    	if (previewStarted && previewMode === "pushing") {
		    	restartPushingPreview();
		    }
	    }
	    
	    
    	/*if (!stopButtonPressed && previewStarted) {
    		console.log(val);
			stopPreviewTimeout();
			startPreviewTimeout(val);			
		}*/
	        
	});
	
	
	
	
	$(document).on('click', '.add-preview-as-location', function(event) {
		var url = $(this).data("url");
		$('.add-preview-as-location').prop('disabled', true);
		createNewLocation(url);
		$('.add-preview-as-location').prop('disabled', false);
	});
	
	
	$('.preview-setting select').change(function () {
		change_preview_setting($(this));
	});
	
	
	$(document).on('click', '#pan-tilt-updater', function (e) {
		var pan = $("input[name='cur_pan']").val();
		var tilt = $("input[name='cur_tilt']").val();
		var url = $(this).data("url");
		var location = $(".group-location .stop-preview:visible").closest(".group-location");
		
		var location_id = location.find("input[name='id']").val();
		
		var data = "pan="+pan+"&tilt="+tilt;
			
		console.log("updating pan:"+pan + ", tilt:"+tilt);
		
		
		if (location_id != "None") {
			data = data + "&id=" + location_id;
			
			
			
		} else {
			/*location.find("input[name='tmp_pan']").val(pan);
			location.find("input[name='tmp_tilt']").val(tilt);*/
			
		}
		
		$.post(url, data).done(function(response) {
			location.find("input[name='pan']").val(pan);
			location.find("input[name='tilt']").val(tilt);
			//$("#group-location-form input[name='pan']").val(pan);
			//$("#group-location-form input[name='tilt']").val(tilt);
			$("#location_pan").html(pan);
			$("#location_tilt").html(tilt);
			displayAlert("success", response);
        });
	});
	
	$('#ptz_goto').click(function() {
	    var url = $(this).data("url");
	    var $wrapper = $(this).closest("#pt-goto-wrapper");
	    var pan = $wrapper.find("#pan_to").val();
	    var tilt = $wrapper.find("#tilt_to").val();
	    
	    console.log("pan: " + pan);
	    console.log("tilt: " + tilt);
	    
	    if (!pan || !tilt) {
	    	return false;
	    }
	    
	    $.post(url, {pan:pan, tilt:tilt}, function(data) {
	    }).done(function(data) {
	    	ptz_post_move(data);
	    });
	});
	
	$('.ptz_move_direction').click(function() {
	    var url = $(this).data("url");
	    $.post(url, function(data) {
	
	    }).done(function(data) {
	    	ptz_post_move(data);
	    });
	});
});


//var startPreview = function(moveButton, callback) {
var startPreview = function(location) {
	
	if (previewStarted) {
		if (location) {
			ptz_move_to_location(location);
		}
		/*if (callback && typeof callback === 'function') {
			callback(moveButton);
		}*/
		return true;
	}
	
	//console.log("startPreview()");
	var url = $('#start-preview-btn').data("url");
	
	var ret = false;
    $.ajax({
        url: url,
        /*global: false,*/
        type: 'POST',
        async:false
    }).done(function(response) {
    	if (!response.success) {
    		displayAlert("danger", response.msg);
    		return false;
    	} 
    	$("#viewing-location-name").html("None");
    	$("#cur_pan").html(response.pan);
        $("#cur_tilt").html(response.tilt);
        
    	stopButtonPressed = false;
    	console.log(response);
        
    	
    	if(typeof(EventSource) !== "undefined") {
    		previewMode = 'pushing';
        	console.log("pushing mode");
            ret = startPushingPreview(location);
            console.log("previewStarted " + previewStarted);
    	} else {
    		console.log("Sorry, your browser does not support server-sent events...");
    		console.log("Starting polling process...");
    		previewMode = 'polling';
    		ret = startPollingPreview(location);
    		//updatePreviewImage();
    	}
    	
    	//previewStarted = ret;
    	
    	
        /*activatePreviewMode(response);*/
        
        /*if (callback && typeof callback === 'function') {
			callback(moveButton);
		}*/
        //startTimer();
        //startPreviewTimeout(interval);
        
    }).fail(ajaxFailCallback); /* !!! not working !!! */
    return ret;
    
    
	/*$.post(url, function(data) {
		//$('.result').html(data);
	}).done(function( data ) {
  		startTimer();
  		console.log(timerId);
      	$("#stop-preview-btn").show();
      	$("#start-preview-btn").hide();
      	$('.ptz_move').attr('disabled', false);
        $('#ptz_goto').attr('disabled', false);
        $('.add-preview-as-location').attr('disabled', false);
        $("#preview-img").css({ opacity: 1 });
        //displayAlert("success", "live-preview has been started");
        console.log("started live-preview");
        return true;
	}).error(function( data ) {
		return false;
	});*/
}


function stopPreview() {
	console.log("stopPreview()");
	previewStarted = false;
	
	if (previewMode == "polling") {
		stopPollingPreview();
	} else if (previewMode == "pushing") {
		stopPushingPreview();
		remoteSavePreviewImage();
	}
	
	
}

var remoteSavePreviewImage = function() {
	var url = $('#stop-preview-btn').data("url");

	$.ajax({
        url: url,
        /*global: false,*/
        type: 'POST',
        /*async:false,*/
        
	}).done(function(data) {
    	//stopPreviewSuccess();
		deactivatePreviewMode();
    }).always(function() {
    	$(".stop-preview").prop('disabled', false);
    	$(".move").prop('disabled', false);
    });
	console.log("stopPreview() finished");
};


var activatePreviewMode = function(location) {
	$("#stop-preview-btn").show();
  	$("#start-preview-btn").hide();
  	$('.ptz_move').attr('disabled', false);
    $('#ptz_goto').attr('disabled', false);
    $('#cur_pan_tilt_wrapper').show();
    $('.add-preview-as-location').attr('disabled', false);
    $("#preview-img").css({ opacity: 1 });
    /*var name = "None";
    if (location) {
    	name = location.find()
    }
    $("#viewing-location-name").html(name);*/
    
    //displayAlert("success", "live-preview has been started");
    //console.log("started live-preview");
};

var deactivatePreviewMode = function() {
	$('.ptz_move').attr('disabled', true);
    $('#ptz_goto').attr('disabled', true);
    $('#cur_pan_tilt_wrapper').hide();
    $("#cur_pan").html();
    $("#cur_tilt").html();
    $('.add-preview-as-location').attr('disabled', true);
    $("#preview-img").css({ opacity: 0.5 });
    $("#stop-preview-btn").hide();
    $('.stop-preview').hide();
    $("#start-preview-btn").show();
    $(".move").show();
    //displayAlert("success", "live-preview has been stopped");
    $("#viewing-location-name").html("Off");
	/*$("#pan-tilt-refresh-wrapper").hide();*/
	$( "#pan-tilt-updater" ).prop( "disabled", true );
	$("#spinner").hide();
};



var startPollingPreview = function(location) {
	previewMode = "polling";
	
	/*if (change_preview_setting_interrupt) {
		stopPreviewTimeout();
		startPreviewTimeout(interval);
		return true;
	}*/
	previewStarted = true;
	if (location)
    	ptz_move_to_location(location);
	console.log("activating preview mode");
	activatePreviewMode(location);
	$("#spinner").hide();
		
	updatePollingPreview();
	return true;
};

var updatePollingPreview = function(location) {
	var url = $('#preview-img').data("url") + '?' + new Date().getTime();
	var startTime = new Date().getTime();
	
	$.ajax({
        type: "GET",
        url: url,
        beforeSend: function() {
        	updateStarted = true;
        	$loading.hide();
        }
    }).fail(function(data) {
    	displayAlert("danger" ,data.responseText);
		console.log("fail: " + data.responseText);
		stopPollingPreview();
		deactivatePreviewMode();
	}).done(function(data) {
		
		var interval = $("#preview_update_interval").val();
		var endTime = new Date().getTime();
		var duration = endTime-startTime;
		console.log(duration);
		if (duration > interval) {
			interval = 0;
		} else {
			interval = interval-duration;
		}
		if (data)
			$('#preview-img').attr('src', "data:image/jpeg;base64,"+data);
		
		if (!previewStarted) {
			remoteSavePreviewImage();
			return true;
		}
		
		//var id = setTimeout(updatePreviewImage, interval);
		var id = setTimeout(updatePollingPreview, interval);
		previewUpdateTimeoutIds.push(id);
		
		/*if (!stopButtonPressed) {
			
			stopPreviewTimeout();
			startPreviewTimeout(interval);			
		} else {
			stopPreview();
		}*/
		//ret = true;
	}).always(function() {
		updateStarted = false;
	});
}


var stopPollingPreview = function() {
	//console.log("clearAllTimeouts()");
	//previewRunning = false;
	//previewStarted = false;
	var length = previewUpdateTimeoutIds.length;
	//console.log(previewUpdateTimeoutIds);
	for (var i = 0; i < length; i++) {
		clearTimeout(previewUpdateTimeoutIds[i]);
	}
	previewUpdateTimeoutIds = new Array();
	
};


var startPushingPreview = function(location) {
	var sse_img_url = $('#preview-img').data("sse-url");
	var interval = $("#preview_update_interval").val();
	if (interval.length == 0) {
		interval = 0;
    }
	$("#spinner").show();
	//console.log(sse_img_url+"?interval="+interval);
    sse_preview_source = new EventSource(sse_img_url+"?interval="+interval);
    previewMode = "pushing";
    sse_preview_source.addEventListener("preview-img", function(e) {
    	if (!previewStarted) {
    		previewStarted = true;
    		if (location)
            	ptz_move_to_location(location);
    		console.log("activating preview mode");
    		activatePreviewMode(location);
    		$("#spinner").hide();
    		
    	}
    	if (!previewRunning) {
    		
    	}
    	previewRunning = true;
    	$('#preview-img').attr('src', "data:image/jpeg;base64,"+e.data);
    });
    
    sse_preview_source.addEventListener('preview-stopped', function(e) {
    	displayAlert("success", e.data);
    	stopPushingPreview();
    	deactivatePreviewMode();
    });
    
    sse_preview_source.addEventListener('open', function(e) {
    	console.log("sse connection established");
    	console.log("previewStarted " + previewStarted);
    });
    
    sse_preview_source.addEventListener('error', function(e) {    	
    	console.log(e.data);
	    if (e.readyState == EventSource.CLOSED) {
	    	console.log(e);
	    	displayAlert("error", e.data);
	    }
	    stopPushingPreview();
    	deactivatePreviewMode();
	}, false);
    
    return true;
};

var stopPushingPreview = function() {
	sse_preview_source.close();
	previewRunning = false;
};

/*var restartPreview = function() {
	if (previewMode === 'polling') {
		
	} else if (previewMode === 'pushing') {
		stopPushingPreview();
		startPushingPreview();
	}
};*/


var restartPushingPreview = function() {
	stopPushingPreview();
	startPushingPreview();
};



function createNewLocation(url) {
	var cur_group_id = $(".group-link.active").data("id");
	console.log("creating new location for group " + cur_group_id);
	var dataobj = {};
	dataobj['group_id'] = cur_group_id;
	
	interruptPreview();
	
	$.ajax({
		url : url,
		data: dataobj,
		type : "POST",
		async:false
	}).done(function(data) {
		if (data.error_msg) {
			displayAlert("error", data.error_msg);
			return false;
		}
		else {
			//scrollTo("#location-section");
			if ( $("#group-locations li").size() > 0) {
				$("#group-locations").append(data);					
			} else {
				$("#group-locations").html(data);
			}
			activateLocation($("#group-locations li:last"), resumePreview);
            //$("#group-locations li:last").click();
		}
        
		return false;
	}).fail(function(data) {
		displayAlert("error", data.responseText);
		resumePreview();
	}).always(function() {
		//resumePreview();
	});
	
	
}




var ptz_post_move = function(msg, location) {
	console.log(msg);
	displayAlert("info", msg);
	
	var pan = "";
	var tilt = "";
	
	/* parsing ptview.py response */
	msg = msg.substring(msg.indexOf("pan=")+4, msg.length - 1);
	pan = msg.substring(0, msg.indexOf(" "));
		
	msg = msg.substring(msg.indexOf("tilt=")+5, msg.length - 1);
	tilt = msg.substring(0, msg.indexOf(" "));
		
	console.log("pan:"+pan + ", tilt:"+tilt);
	
	$("#cur_pan").html(pan);
	$("#cur_tilt").html(tilt);
	$("#pan-tilt-control input[name='cur_pan']").val(pan);
	$("#pan-tilt-control input[name='cur_tilt']").val(tilt);
	
	if (location) {
		$(".group-location .stop-preview").hide();
		$(".group-location .stop-preview").prev().show();
		
		location.find(".move").hide();
		location.find(".stop-preview").show();
		location.click();
	}
}

var ptz_move_to_location = function(location) {
	var moveButton = location.find('.move');
	var url = moveButton.data("url");
	var pan = location.find("input[name='pan']").val();
	var tilt = location.find("input[name='tilt']").val();
	
	$.post(url, {pan:pan, tilt:tilt}, function(data) {	
    }).done(function(data) {
		console.log("moved to location");
		var viewing_location_name = location.find("input[name='name_full']").val();
		if (!viewing_location_name) {
			viewing_location_name = "unsaved location";
		}
		$("#viewing-location-name").html(viewing_location_name);
		$( "#pan-tilt-updater" ).prop( "disabled", false );
		ptz_post_move(data, location);    		
    });
}

var ptz_post_move_call = function(button) {
	var location = $(button).closest(".group-location");
	var url = $(button).data("url");
	var viewing_location_name = location.find("input[name='name_full']").val();
	if (!viewing_location_name) {
		viewing_location_name = "unsaved location";
	}
	var pan = location.find("input[name='pan']").val();
	var tilt = location.find("input[name='tilt']").val();
	location.click();
	
	$.post(url, {pan:pan, tilt:tilt}, function(data) {
		
    }).done(function(data) {
		console.log("move to location");
		/*$("#viewing-location").show();*/
		$("#viewing-location-name").html(viewing_location_name);
		/*$("#pan-tilt-refresh-wrapper").show();*/
		$( "#pan-tilt-updater" ).prop( "disabled", false );
		ptz_post_move(data);    		
    });
}


var change_preview_setting = function(select) {
	
	var name = select.data("name");
	var new_val = select.val();
	//var config = select.closest(".preview-setting").find("input[name='config']").val();
	//var new_index = select[0].selectedIndex;
	
    var preview_interval = $("#preview_update_interval").val();
    if (preview_interval.length == 0) {
    	preview_interval = 500;
    }
    interruptPreview();
    
    
    setTimeout(function(){
    	$.ajax({
	        type: "POST",
	        url: select.data("url"),
	        data: {name:name, value:new_val},
	        dataType: 'json',
	        success: function(data) {
	        	if (data.success) {
	        		displayAlert("success", data.msg);
	        	} else {
	        		displayAlert("danger", data.msg);
	        	}
	        }
	    }).always(function() {
	    	resumePreview();
	    });
    }, preview_interval);
};

var interruptPreview = function() {
	change_preview_setting_interrupt = true;
    if (previewStarted) {
    	console.log("interrupting preview");
    	if (previewMode === 'polling') {
	    	stopPollingPreview();
	    } else if (previewMode === 'pushing') {
	    	stopPushingPreview();
	    } else {console.log("interrupt without previewMode");}
    }
};

var resumePreview = function() {
	change_preview_setting_interrupt = false;
	if (previewStarted) {
		console.log("resuming preview");
    	if (previewMode === 'polling') {
	    	startPollingPreview();
	    } else if (previewMode === 'pushing') {
	    	startPushingPreview();
	    }
	}
};